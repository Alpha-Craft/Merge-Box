import pygame
from pygame.locals import *
import pygame_gui as gui
from time import time
from random import randint, sample
from debug import debug_push, debug_update
from box import Box, boxes

pygame.init()
screen = pygame.display.set_mode((1000, 750), flags=RESIZABLE | SCALED)
pygame.display.set_caption("Merge-Box dev")
pygame.display.set_icon(pygame.image.load("img/icon.png").convert_alpha())
screen_rect = screen.get_rect()
clock = pygame.time.Clock()

cursor = pygame.Surface((10, 10)).convert_alpha()
cursor.fill((0, 0, 0, 0))
pygame.draw.circle(cursor, (255, 255, 255), (5, 5), 5)
pygame.mouse.set_cursor((5, 5), cursor)

show_debug = True

# manager = gui.UIManager((1000, 750))
#
# button_layout_rect = pygame.Rect(0, 0, 100, 20)
# button_layout_rect.bottomright = (-30, -20)
#
# pygame.Rect(screen_rect.width // 2 - 50, screen_rect.height // 2 - 25, 100, 50)
#
# hello_button = gui.elements.UIButton(relative_rect=button_layout_rect,
#                                      text="Say Hello",
#                                      manager=manager,
#                                      anchors={"left": "right",
#                                               "right": "right",
#                                               "top": "bottom",
#                                               "bottom": "bottom"})

run = True
dt = 0
time_delta = 0
last_frame = time()

Box()
Box()

while run:
    dt = (time() - last_frame) * 60
    last_frame = time()

    for event in pygame.event.get():
        if event.type == QUIT:
            run = False
        if event.type == MOUSEBUTTONDOWN:
            if event.button == 1:
                for box in boxes:
                    if box.actual_rect.collidepoint(event.pos):
                        box.clicked = True
        if event.type == MOUSEBUTTONUP:
            if event.button == 1:
                for box in boxes:
                    box.clicked = False
        if event.type == KEYDOWN:
            if event.key == K_F3:
                show_debug = not show_debug

        # if event.type == gui.UI_BUTTON_PRESSED:
        #     if event.ui_element == hello_button:
        #         print("Hello World!")

        # manager.process_events(event)

    screen.fill((0, 0, 0))
    # manager.update(dt)

    # manager.draw_ui(screen)

    # for box_index, box in enumerate(boxes):
    #     box.update()
    #     pygame.draw.rect(screen, (255, 0, 0), box.visual_rect)
    #     if show_debug:
    #         debug_push(f"Box {box_index + 1} {('not ', '')[box.clicked]}clicked")

    boxes.update(dt, show_debug)
    boxes.draw(screen)

    debug_update()
    pygame.display.update()
    time_delta = clock.tick(60)  # / 1000.0
pygame.quit()
