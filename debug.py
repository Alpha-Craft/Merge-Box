import pygame

pygame.init()
font = pygame.font.Font(None, 30)
debug_info = []


def debug_push(info):
    debug_info.append(info)


def debug_update():
    for e_index, e in enumerate(debug_info):
        debug(e, 10, e_index * 25 + 10)
    debug_info.clear()


def debug(info, x=10, y=10):
    display_surf = pygame.display.get_surface()
    debug_surf = font.render(str(info), True, (255, 255, 255))
    debug_rect = debug_surf.get_rect(topleft=(x, y))
    pygame.draw.rect(display_surf, (0, 0, 0), debug_rect)
    display_surf.blit(debug_surf, debug_rect)
