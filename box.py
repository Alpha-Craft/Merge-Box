import pygame
from pygame.locals import *
from random import randint
from debug import debug_push
from time import time

clicked = False
last_clicked = None
boxes = pygame.sprite.Group()


class Box(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.pos = [randint(0, 950), randint(0, 700)]
        self.visual_pos = self.pos.copy()
        self.clicked = False
        self.screen_rect = pygame.display.get_surface().get_rect()
        boxes.add(self)
        self.box_index = list(boxes.spritedict.keys()).index(self)

    @property
    def actual_rect(self):
        return Rect(*self.pos, 50, 50)

    @property
    def rect(self):
        return Rect(*self.visual_pos, 50, 50)

    @property
    def center(self):
        return pygame.Vector2(self.actual_rect.center)

    def check_for_collisions(self):
        for _box_ in boxes:
            if pygame.sprite.collide_rect(self, _box_):
                return True
        return False

    def update(self, delta_time, show_debug):
        global clicked, last_clicked

        if not clicked:  # or (clicked and self.check_for_collisions() and last_clicked == self)
            if self.clicked:
                clicked = True
                last_clicked = self
                self.pos = pygame.mouse.get_pos()[0] - 25, pygame.mouse.get_pos()[1] - 25
            rect = self.actual_rect.clamp(self.screen_rect)
            self.pos = rect[0:2]
        elif clicked:
            self.clicked = False

        if show_debug:
            debug_push(pygame.Vector2(self.pos) - pygame.Vector2(self.visual_pos))

        self.visual_pos += pygame.Vector2((pygame.Vector2(self.pos) - pygame.Vector2(self.visual_pos)) / 4 * delta_time)
        self.visual_pos[0] = (self.visual_pos[0], self.pos[0])[abs(self.pos[0] - self.visual_pos[0]) <= 0.5]
        self.visual_pos[1] = (self.visual_pos[1], self.pos[1])[abs(self.pos[1] - self.visual_pos[1]) <= 0.5]

        for box_ in boxes:
            if box_ != self and not self.clicked:
                if pygame.sprite.collide_rect(self, box_):
                    avoid_vector = box_.center - self.center
                    print(f"Unchanged {self.box_index} {avoid_vector}")
                    if avoid_vector == pygame.Vector2(0, 0):
                        if self.pos[0] >= 500:
                            avoid_vector.x = 240
                        else:
                            avoid_vector.x = -240

                        if self.pos[1] >= 375:
                            avoid_vector.y = 240
                        else:
                            avoid_vector.y = -240

                    if abs(avoid_vector.x) >= 6 or abs(avoid_vector.y) >= 6:
                        avoid_vector.x /= 6
                        avoid_vector.y /= 6
                        print("yo")
                    elif abs(avoid_vector.x) < 6 and abs(avoid_vector.y) < 6:
                        avoid_vector *= 6
                        print("no")

                    print(f"Changed   {self.box_index} {avoid_vector}")
                    self.pos -= avoid_vector
                    rect = self.actual_rect.clamp(self.screen_rect)
                    self.pos = rect[0:2]

        if show_debug:
            debug_push(f"Box {self.box_index + 1} {('not ', '')[self.clicked]}clicked")
            debug_push(f"Pos {self.box_index + 1}: {self.pos}; {self.visual_pos}")

        if self.box_index == len(boxes) - 1:
            clicked = False
            last_clicked = None
            if show_debug:
                debug_push(f"DeltaTime: {delta_time}")

    @property
    def image(self):
        if self.box_index == len(boxes.spritedict) - 1:
            sur = pygame.Surface((50, 50))
            pygame.draw.rect(sur, (0, 255, 0), (0, 0, 50, 50))
        else:
            sur = pygame.Surface((50, 50))
            pygame.draw.rect(sur, (255, 0, 0), (0, 0, 50, 50))
        return sur
