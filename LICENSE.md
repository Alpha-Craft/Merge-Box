# License
All art and code in this repo may only be used with proper mentioning of the creator, [Alpha-Craft](https://gitlab.com/Alpha-Craft). (CC BY-SA 3.0)
All changes have to be put under the same license.
